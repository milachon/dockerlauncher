# Prerequisites

docker installed
golang sdk installed

## HOTFIX
Need login to dockerhub to use scan

Execute: 
    docker login

# Run
Execute: 
    
    go run . <dockerfile_path>

A test file is available at "./dockerfile_test/test_file_docker_1"

# State
The program do the followings: 

    * copy input dockerfile path into a temp directory
    * build through docker compose the docker 
    * scan the built image with docker scan 
    * run the built image (or rebuild it)
    * read the perf value and set it with status in list
    * remove tmp files

Current bug list: 
    * unmarshall system doesn't understand {'perf': 0.99}, only with double quote {"perf": 0.99}

Next step:
    
    * Rest API layer
    * data layer
    * improve security
    * industrialisation
