package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	guuid "github.com/google/uuid"
)

type Status string

const (
	SUCCESS Status = "SUCCESS"
	FAIL           = "FAIL"
)

// low, medium, high
const scanSeverity = "high"

const tmpFolder = "tmp"

type Job struct {
	Status Status
	Perf   float32
}

var jobsStatusById map[string]Job

// use: go run dockerlauncher <input_dockerfile_path>
func main() {
	// Check if docker-compose is installed and in path
	_, err := exec.LookPath("docker-compose")
	if err != nil {
		fmt.Printf("Docker compose not in path\n")
		log.Fatal(err)
	}

	if len(os.Args) < 2 {
		fmt.Printf("Need path to a dockerfile as argument\n")
		log.Fatal(err)
	}

	// generate id to reference the incoming dockerfile
	fileUUID := guuid.NewString()

	// create temp dockerfile dir
	if _, err := os.Stat(tmpFolder); os.IsNotExist(err) {
		os.Mkdir(tmpFolder, os.ModeDir)
	}
	if _, err := os.Stat(tmpFolder + "/" + fileUUID); os.IsNotExist(err) {
		os.Mkdir(tmpFolder+"/"+fileUUID, os.ModeDir)
	}

	// copy incoming dockerfile into temp context folder
	// FIXME is there a way to pass dockerfile content directly ?
	_, err = copy(os.Args[1], tmpFolder+"/"+fileUUID+"/"+fileUUID)
	if err != nil {
		fmt.Printf("Copy incoming dockerfile failed\n")
		log.Fatal(err)
	}

	// process the file like if it is coming from rest layer
	manageIncomingDockerfile(fileUUID)

	fmt.Printf("Result: \n")
	fmt.Print(fileUUID)
	fmt.Println(jobsStatusById[fileUUID])

	// remove temp file
	err = os.RemoveAll(tmpFolder + "/" + fileUUID)
	if err != nil {
		fmt.Printf("Remove temporary file failed\n")
		log.Fatal(err)
	}
}

func manageIncomingDockerfile(fileUUID string) {

	// simulate persisting data with the map
	jobsStatusById = make(map[string]Job)

	// Use . as context
	ctx := context.Background()

	// preset status
	status := SUCCESS

	err := buildImage(ctx, fileUUID, fileUUID)
	if err != nil {
		fmt.Printf("Docker compose build failed\n")
		status = FAIL
		log.Print(err)
	} else {
		fmt.Printf("Docker compose build success\n")
	}

	err = scanBuiltImage(ctx, fileUUID)
	if err != nil {
		fmt.Printf("Docker scan failed\n")
		status = FAIL
		log.Print(err)
	} else {
		fmt.Printf("Docker scan success\n")
	}

	err = runImage(ctx, fileUUID, fileUUID)
	if err != nil {
		fmt.Printf("Docker compose up (Run) failed\n")
		status = FAIL
		log.Print(err)
	} else {
		fmt.Printf("Docker compose up (Run) success\n")
	}

	// store the job id, the status and perf if available
	result, err := readResultJson(fileUUID)
	if err != nil {
		fmt.Printf("Failed to read perf.json\n")
		log.Print(err)
	} else {
		fmt.Printf("Perf is %g\n", result.Perf)
	}
	jobsStatusById[fileUUID] = Job{status, result.Perf}
}

// build docker through docker-compose
func buildImage(ctx context.Context, fileUUID string, dockerfileName string) error {

	cmd := exec.CommandContext(ctx, "docker-compose", "build")

	// passing reference for current call
	cmd.Env = append(os.Environ(),
		"DOCKER_FILE="+dockerfileName,
		"IMAGE_NAME="+fileUUID,
		"BUILD_CONTEXT="+tmpFolder+"/"+fileUUID,
	)
	err := cmd.Run()
	return err
}

// scan built image thanks to beta docker scan function
func scanBuiltImage(ctx context.Context, fileUUID string) error {

	// WARNING need to be logged before use scan
	// --login option can be used in scan with a token
	err := dockerLogin()
	if err != nil {
		fmt.Printf("Docker login failed, can't continue scan.")
		return err
	}

	cmd := exec.CommandContext(ctx, "docker", "scan", "--severity", scanSeverity, fileUUID)

	// redirect output of scan tools
	var out bytes.Buffer
	cmd.Stdout = &out

	if err := cmd.Run(); err != nil {
		fmt.Println("Error: " + out.String())
	}
	return err
}

// login to dockerhub
// necessary to use scan
func dockerLogin() error {
	// TODO
	// use env variables to log to docker hub
	// cmd := exec.CommandContext(ctx, "docker", "login")
	// if err := cmd.Run(); err != nil {
	// 	log.Print(err)
	// }
	return nil
}

// run docker image
func runImage(ctx context.Context, imageName string, dockerfileName string) error {
	cmd := exec.CommandContext(ctx, "docker-compose", "up")

	// passing reference for current call
	cmd.Env = append(os.Environ(),
		"DOCKER_FILE="+dockerfileName,
		"IMAGE_NAME="+imageName,
		"BUILD_CONTEXT="+tmpFolder+"/"+imageName,
	)

	err := cmd.Run()
	return err
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

type Result struct {
	Perf float32 `json:"perf"`
}

func readResultJson(fileUUID string) (Result, error) {
	var result Result

	jsonFile, err := os.Open(tmpFolder + "/" + fileUUID + "/perf.json")
	if err != nil {
		return result, err
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)

	// FIXME it accepts only double quote for key
	json.Unmarshal(byteValue, &result)
	jsonFile.Close()
	return result, nil
}
