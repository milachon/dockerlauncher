## Table of Contents
1. [General Project Info](#general-project-info)
2. [Conception](#conception)
3. [Proof of concept](#proof-of-concept)
4. [Development tier](#development-tier)
5. [Production tier](#production-tier)
5. [Personnal comment](#personnal-comment)

### General Project Info

#### Requirement (in French)

    Construire un service en Python ou Go exposant une API REST, qui permet de:

    1. soumettre un dockerfile et retourner un identifiant de job.
    Ce dockerfile réalise une tâche et sauve une performance (un décimal) dans un json
    (situé dans un volume appelé `data` monté au docker). Un exemple d'un tel Dockerfile est fourni ci dessous.
    Une fois le dockerfile reçu, le service build l'image docker, fais un scan de ses vulnérabilités, puis soit associe au job un statut fail, soit run le docker dans un environnement le plus sécurisé possible, et associe le résultat (le décimal) au job et un status success.

    2. récupérer le statut et possiblement la performance (le décimal) associés au job.

##### Contenu du Dockerfile :
```
FROM ubuntu:16.04
# train machine learning model
# save performances
CMD echo "{'perf':0.99}" > data/perf.json
```
### Conception

#### 1. Define main or most problematic features (by priority)

    1. Interact with Docker Engine from code 

    2. Scan vulnerabilities of a Dockerfile
    
    3. Run Dockerfile in most secure way
    
    4. Get back data from a docker
    
    5. Open a REST Api to access features
    
#### 2. Proposal and state of the art
    
##### 1. Interact with Docker Engine from code
    - Search way to do it 
        * for PoC, system call can be used
        * for prod, official Docker sdk for golang available

##### 2. Scan vulnerabilities of a Dockerfile
    - Search for best practice / ideas
        Result: 
        - no port forwarding
        - no COPY DELETE command
        - do not run docker as root
        - ... (TBD)
    - Search for best open source tool
        Result: 
        - docker scan (enough for PoC)
            * Pros:
                - official 
            * Cons:
                - but beta release
                - need third party access (Snyk)
                - limited scan number without auth (see to use locally)
        - Anchore engine
            * Pros:
                - advanced features
            * Cons:
                -  thirs party access
        - homemade scan
            * Pros: 
                - local launch
                - follow our needs
            * Cons: 
                - based on our own knowloedge or search about docker security issue
                - probably very long and fastidious to develop -> bug
        - ... (TBD)

##### 3. Run Dockerfile in most secure way
    - Search for best practice / ideas
        Result: 
        - run imported container in a container defined by the service (with docker-compose ?)
        (this add a virtual layer to be sure to not expose unauthorized server ports or files)
        - ... (TBD)

##### 4. Get back data from a docker
    No specific work here, get output of read file command from docker engine

##### 5. Open a REST Api to access features
    No specific work here, use usual REST layer used in other services

### Proof of concept
#### 2.1. Choose language between Python and Go
    -> Go (no particular reason for this usecase)
#### 2.2. Write a simple application 
##### which in order of features:
    1. execute system calls
    2. read a file
    3. run docker scan on a file
    4. run a docker file
    5. get file content from the docker ('data/perf.json')

### Development tier
Upgrade application or rewrite to development tier
#### 3.1. Configure simple CI environment (gitlab-ci by example)
    - auto tests run
    - code quality analysis
    - integration tests ? 
#### 3.2. Generate/prepare web project
    - use usual template like MVC
    - add swagger API documentation autogeneration
    -> ease test and documentation is always updated
#### 3.3.1 Write the asked Rest layer with stub (independant task)
    - write mock unitary test
    - Something in particular to notice ?
##### Test plan
    - ... (TBD)
#### 3.3.2 Use go Docker sdk instead of system calls (independant task)
    - if possible for every needs
    - write unitary tests
##### Test plan
    - scan a bad file -> fail
    - scan a good file -> success
    - run a good file with perf -> success and perf equals
    - run a good file without perf -> success 
#### 3.4. Add a database system
    -> PostGreSQL ? The one used generally by the team
#### 3.5. Integrate the Rest layer and data layer to service layer
    - write integration tests
##### Test plan
    TBD

### Production tier
Upgrade application to production tier
#### 4.1 Security
    - Security layer on Rest API
        -> use usual one or defined this in another project
    - Challenge the scan function
        -> special use of external security expert ?

#### 4.2 Quality of Service
    - Deploy application in managed docker orchestrator
        -> see with devOps
    - Manage exceptions

#### 4.3 Maintainability
    - Update the documentation !!!

### Personnal comment
    With more time I could: 
    - not skip so fast the search of best practice in docker security matters, skipped here to focus on other stuff
    - beautify this pre conception document and add schemes
    - develop/do more of the above tasks (first one code structuration)
    - discuss about and upgrade this plan before going in it with team
    - get a coffee

    Time spent: 
    - report 1h30
    - code 5h (time pass so fast)